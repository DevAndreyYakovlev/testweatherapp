package com.ex.weatherapp.ui.weather_list.view;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.ex.weatherapp.data.entity.weather.WeatherEntity;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Yakovlev Andrey. 8/14/2017
 */

public class MyItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private PublishSubject<WeatherEntity> mOnSwipedItem = PublishSubject.create();

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        // Задайте флагам значение 0, если они не должны перемещаться
        int swipeFlags = 0;
        int dragFlags = 0;
        // Если имеем дело с обычным итемом, то задаём флаги движения вправо и влево
        if (viewHolder instanceof WeatherRecyclerViewAdapter.MyViewHolder)
            swipeFlags = ItemTouchHelper.START;

        // Используем стандартный метод для создания битмаски флагов движения
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    public PublishSubject<WeatherEntity> getSwipedItemObs(){
        return mOnSwipedItem;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        mOnSwipedItem.onNext(((WeatherRecyclerViewAdapter.MyViewHolder) viewHolder).mWeatherEntity);
        System.out.println();
    }
}
