package com.ex.weatherapp.ui.weather_list.presenter;

import com.ex.weatherapp.business.weather_list.IWeatherListInteractor;
import com.ex.weatherapp.data.entity.weather.WeatherEntity;
import com.ex.weatherapp.ui.weather_list.view.IWeatherView;
import com.ex.weatherapp.utils.MyLocation;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public class WeatherListPresenter implements IWeatherListPresenter {


    private final IWeatherListInteractor mWeatherListInteractor;
    private IWeatherView mWeatherView;

    public WeatherListPresenter(IWeatherListInteractor weatherListInteractor) {
        mWeatherListInteractor = weatherListInteractor;
    }

    @Override
    public void bindView(IWeatherView weatherView) {
        mWeatherView = weatherView;
    }

    @Override
    public void unbindView() {

    }

    @Override
    public void loadWeatherList() {
        mWeatherListInteractor.loadWeatherList()
            .subscribe(weatherEntities -> {
                mWeatherView.showWeatherList(weatherEntities);
            });
    }

    @Override
    public void loadWeatherForMyCity(MyLocation myLocation) {
        if (mWeatherListInteractor.loadWeatherForMyLocation(myLocation) != null){
            mWeatherListInteractor.loadWeatherForMyLocation(myLocation)
                .subscribe(weatherEntity -> {
                    mWeatherView.addMyCityWeather(weatherEntity);
                });
        }
    }

    @Override
    public void updateWeatherList() {
        mWeatherListInteractor.updateData()
                .subscribe(weatherEntities -> {
                    mWeatherView.showUpdateWeatherList(weatherEntities);
                }, throwable -> {
                    System.out.println();
                });
    }

    @Override
    public void deleteCity(WeatherEntity weatherEntity) {
        mWeatherListInteractor.deleteCity(weatherEntity);
    }
}
