package com.ex.weatherapp.ui.weather_list.view;

import com.ex.weatherapp.data.entity.weather.WeatherEntity;

import java.util.List;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public interface IWeatherView {

    void showWeatherList(List<WeatherEntity> objectses);
    void addMyCityWeather(WeatherEntity weatherEntity);
    void showErrorMessage(String errMsg);
    void showUpdateWeatherList(List<WeatherEntity> weatherEntities);
}
