package com.ex.weatherapp.ui.splash_screen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ex.weatherapp.R;
import com.ex.weatherapp.ui.weather_list.view.WeatherActivity;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        new Handler().postDelayed(() -> {
            startActivity(new Intent(this, WeatherActivity.class));
            finish();
        },1000);

    }
}
