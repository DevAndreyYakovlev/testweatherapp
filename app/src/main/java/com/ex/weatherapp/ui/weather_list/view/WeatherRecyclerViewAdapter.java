package com.ex.weatherapp.ui.weather_list.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ex.weatherapp.R;
import com.ex.weatherapp.data.entity.weather.WeatherEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yakovlev Andrey. 8/13/2017
 */

public class WeatherRecyclerViewAdapter extends RecyclerView.Adapter<WeatherRecyclerViewAdapter.MyViewHolder> {

    private final List<WeatherEntity> mData;

    public WeatherRecyclerViewAdapter() {
        this.mData = new ArrayList<>();
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_recycler_item, parent, false);
        return new MyViewHolder(view);
    }

    public void addCityWeather(WeatherEntity weatherEntity){
        mData.add(0,weatherEntity);
        notifyItemInserted(0);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        WeatherEntity weatherEntity = mData.get(position);
        holder.mWeatherEntity = weatherEntity;
        int temp = Math.round(weatherEntity.getMain().getTemp());
        int windSpeed =  Math.round(weatherEntity.getWind().getSpeed());
        holder.mCityName.setText(weatherEntity.getName());
        holder.mCityTemp.setText(String.valueOf(temp));
        String wind = String.valueOf(windSpeed) + " м/с, " + weatherEntity.getWind().getDescription();
        holder.mCityWind.setText(wind);

    }



    public void addWeatherList(List<WeatherEntity> weatherEntities) {
        mData.clear();
        mData.addAll(weatherEntities);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cityTemp) TextView mCityTemp;
        @BindView(R.id.cityName) TextView mCityName;
        @BindView(R.id.cityWind) TextView mCityWind;
        WeatherEntity mWeatherEntity;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
