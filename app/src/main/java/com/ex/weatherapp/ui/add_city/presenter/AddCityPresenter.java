package com.ex.weatherapp.ui.add_city.presenter;


import com.ex.weatherapp.business.add_city.IAddCityInteractor;
import com.ex.weatherapp.ui.add_city.view.IAddCityView;
import com.google.android.gms.location.places.Place;

/**
 * Created by Yakovlev Andrey. 8/13/2017
 */

public class AddCityPresenter implements IAddCityPresenter {
    private final IAddCityInteractor mAddCityInteractor;
    private IAddCityView mAddCityView;

    public AddCityPresenter(IAddCityInteractor addCityInteractor) {
        mAddCityInteractor = addCityInteractor;
    }

    @Override
    public void bindView(IAddCityView addCityView) {
        mAddCityView = addCityView;
    }

    @Override
    public void unbindView() {

    }


    @Override
    public void onPlaceSelected(Place place) {
        mAddCityView.showProgressBar();
        mAddCityInteractor.addCityByPlace(place)
                .subscribe(aBoolean -> {
                    mAddCityView.hideProgressBar();
                    System.out.println();
                }, throwable -> {
                    mAddCityView.showError("Ooops. Что-то пошло не так");
                    System.out.println();
                });
        System.out.println();
    }
}
