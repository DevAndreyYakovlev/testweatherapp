package com.ex.weatherapp.ui.add_city.presenter;

import com.ex.weatherapp.ui.add_city.view.IAddCityView;
import com.google.android.gms.location.places.Place;

/**
 * Created by Yakovlev Andrey. 8/13/2017
 */

public interface IAddCityPresenter {

    void bindView(IAddCityView addCityView);
    void unbindView();
    void onPlaceSelected(Place place);

}
