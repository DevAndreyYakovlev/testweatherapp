package com.ex.weatherapp.ui.weather_list.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.FrameLayout;

import com.ex.weatherapp.R;
import com.ex.weatherapp.WeatherApplication;
import com.ex.weatherapp.data.entity.weather.WeatherEntity;
import com.ex.weatherapp.di.weater_list.WeatherListModule;
import com.ex.weatherapp.ui.add_city.view.AddCityActivity;
import com.ex.weatherapp.ui.weather_list.presenter.IWeatherListPresenter;
import com.ex.weatherapp.utils.MyLocation;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public class WeatherActivity extends AppCompatActivity implements IWeatherView {

    @Inject
    IWeatherListPresenter mWeatherListPresenter;
    @BindView(R.id.weatherRecycler)
    RecyclerView mWeatherRecyclerView;
    @BindView(R.id.toolbar)
    Toolbar mToolBar;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.prgressBarContainer)
    FrameLayout mProgressBarContainer;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private WeatherRecyclerViewAdapter mWeatherRecyclerViewAdapter;
    private boolean mDataIsLoaded;
    FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark, this.getTheme()));
        }
        fab = (FloatingActionButton) findViewById(R.id.fab);

        WeatherApplication.get(getApplicationContext()).applicationComponent().plus(new WeatherListModule()).inject(this);
        ButterKnife.bind(this);
        mWeatherListPresenter.bindView(this);

        mWeatherRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mWeatherRecyclerViewAdapter = new WeatherRecyclerViewAdapter();
        mWeatherRecyclerView.setAdapter(mWeatherRecyclerViewAdapter);
        ItemTouchHelper.Callback callback = new MyItemTouchHelperCallback();

        ((MyItemTouchHelperCallback) callback).getSwipedItemObs()
                .subscribe(weatherEntity -> {
                    mWeatherListPresenter.deleteCity(weatherEntity);
                });

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(mWeatherRecyclerView);


        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mWeatherListPresenter.updateWeatherList();
        });

        fab.setOnClickListener(view -> {
            startActivityForResult((new Intent(this, AddCityActivity.class)), 1);
        });

        initToolBar();
        mWeatherListPresenter.loadWeatherList();
        initCheckPermission();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }
        String result = data.getStringExtra("result");
        if (result.equals("update")) {
            mWeatherListPresenter.loadWeatherList();
        }
    }

    private void initToolBar() {
        setSupportActionBar(mToolBar);
        getSupportActionBar().setTitle("Погода!");
    }

    private void initCheckPermission() {

        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions
                .request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        this.showWeatherForMyLocation();
                    } else {
                        // Oups permission denied
                        mSwipeRefreshLayout.setRefreshing(false);
                        Snackbar.make(fab, getResources().getString(R.string.denyPermission), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                });
    }

    @Override
    public void showErrorMessage(String errMsg) {
        mProgressBarContainer.setVisibility(View.GONE);
        Snackbar.make(fab, "Что-то пошло не так", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    private void showWeatherForMyLocation() {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
        if (!mDataIsLoaded) {
            mProgressBarContainer.setVisibility(View.VISIBLE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(command -> {
                try {
                    List<Address> fromLocation = geocoder.getFromLocation(command.getLatitude(), command.getLongitude(), 1);
                    Address address = fromLocation.get(0);
                    this.mWeatherListPresenter.loadWeatherForMyCity(new MyLocation() {{
                        setLat(address.getLatitude());
                        setLng(address.getLongitude());
                    }});
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }


    @Override
    public void showWeatherList(List<WeatherEntity> weatherEntities) {
        if (weatherEntities.size() > 0) {
            mDataIsLoaded = true;
        }
        mWeatherRecyclerViewAdapter.addWeatherList(weatherEntities);
    }

    @Override
    public void addMyCityWeather(WeatherEntity weatherEntity) {
        mProgressBarContainer.setVisibility(View.GONE);
        mWeatherRecyclerViewAdapter.addCityWeather(weatherEntity);
    }

    @Override
    public void showUpdateWeatherList(List<WeatherEntity> weatherEntities) {
        mSwipeRefreshLayout.setRefreshing(false);
        Snackbar.make(fab, "Данные обновились", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();

        mWeatherRecyclerViewAdapter.addWeatherList(weatherEntities);
    }
}
