package com.ex.weatherapp.ui.add_city.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.ex.weatherapp.R;
import com.ex.weatherapp.WeatherApplication;
import com.ex.weatherapp.di.add_city.AddCityModule;
import com.ex.weatherapp.ui.add_city.presenter.IAddCityPresenter;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Yakovlev Andrey. 8/13/2017
 */

public class AddCityActivity extends AppCompatActivity implements IAddCityView {

    @Inject
    IAddCityPresenter mAddCityPresenter;
    @BindView(R.id.progressBarContainer) FrameLayout mProgressBarContainer;
    private Place mPlace;
    private boolean isSuccess;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_city_activity);
        WeatherApplication.get(getApplicationContext()).applicationComponent().plus(new AddCityModule()).inject(this);
        ButterKnife.bind(this);
        mAddCityPresenter.bindView(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark, this.getTheme()));
        }
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected mPlace.
                mPlace = place;
                Log.i("api", "Place: " + place.getName());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("api", "An error occurred: " + status);
            }
        });
    }

    @OnClick(R.id.submitBtn)
    void onClickSubmit(View v){
        if (mPlace != null)
            mAddCityPresenter.onPlaceSelected(this.mPlace);
    }

    @Override
    public void showProgressBar() {
        mProgressBarContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        isSuccess = true;
        Toast.makeText(this, "Город был добавлен", Toast.LENGTH_SHORT).show();
        mProgressBarContainer.setVisibility(View.GONE);
    }

    @Override
    public void showError(String err) {
        mProgressBarContainer.setVisibility(View.GONE);
        Toast.makeText(this, err, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onBackPressed() {

        String res = "";
        if (isSuccess){
            res = "update";
        }else{
            res = "not_update";
        }
        Intent intent = getIntent();
        intent.putExtra("result", res);
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }
}
