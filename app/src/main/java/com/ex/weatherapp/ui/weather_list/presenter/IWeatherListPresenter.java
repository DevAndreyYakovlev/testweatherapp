package com.ex.weatherapp.ui.weather_list.presenter;

import com.ex.weatherapp.data.entity.weather.WeatherEntity;
import com.ex.weatherapp.ui.weather_list.view.IWeatherView;
import com.ex.weatherapp.utils.MyLocation;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public interface IWeatherListPresenter {

    void bindView(IWeatherView weatherView);
    void unbindView();
    void loadWeatherList();

    void loadWeatherForMyCity(MyLocation myLocation);

    void updateWeatherList();

    void deleteCity(WeatherEntity weatherEntity);
}
