package com.ex.weatherapp.ui.add_city.view;

/**
 * Created by Yakovlev Andrey. 8/13/2017
 */

public interface IAddCityView {

    void showProgressBar();
    void hideProgressBar();
    void showError(String err);

}
