package com.ex.weatherapp.utils;

import com.ex.weatherapp.data.entity.weather.WeatherEntity;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public interface WeatherApiService {

    String WEATHER_API_KEY = "3b59f01739bc541a3d1338c9b67a36c4";

    @GET("data/2.5/weather")
    Observable<WeatherEntity> getCityWeather(@QueryMap Map<String, String> json);

}
