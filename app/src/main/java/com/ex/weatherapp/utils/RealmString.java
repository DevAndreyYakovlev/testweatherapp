package com.ex.weatherapp.utils;

import io.realm.RealmObject;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public class RealmString extends RealmObject {

    public String val;

    public RealmString(String s) {
        val = s;
    }

    public RealmString(){

    }

}
