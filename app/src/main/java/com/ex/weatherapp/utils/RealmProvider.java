package com.ex.weatherapp.utils;

import io.realm.Realm;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public class RealmProvider {

    public RealmProvider() {
    }

    public Realm provideRealm(){
        return Realm.getDefaultInstance();
    }

}
