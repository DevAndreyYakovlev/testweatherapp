package com.ex.weatherapp.utils;

/**
 * Created by Yakovlev Andrey. 8/12/2017
 */

public class MyLocation {

    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
