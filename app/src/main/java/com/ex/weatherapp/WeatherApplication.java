package com.ex.weatherapp;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.ex.weatherapp.di.application.AppComponent;
import com.ex.weatherapp.di.application.AppModule;
import com.ex.weatherapp.di.application.DaggerAppComponent;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public class WeatherApplication extends Application {

    @SuppressWarnings("NullableProblems")
    @NonNull
    private AppComponent appComponent;

    public static WeatherApplication get(@NonNull Context context){
        return (WeatherApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = prepareAppComponent().build();
        RealmConfiguration realmConfig = new RealmConfiguration.Builder(this).deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(realmConfig);
    }


    @NonNull
    private DaggerAppComponent.Builder prepareAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this));
    }

    @NonNull
    public AppComponent applicationComponent() {
        return appComponent;
    }
}
