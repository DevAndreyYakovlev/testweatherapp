package com.ex.weatherapp.business.add_city;

import com.ex.weatherapp.business.weather_list.IWeatherListInteractor;
import com.google.android.gms.location.places.Place;

import io.reactivex.Observable;


/**
 * Created by Yakovlev Andrey. 8/13/2017
 */

public class AddCityInteractor implements IAddCityInteractor {


    private final IWeatherListInteractor mWeatherListInteractor;

    public AddCityInteractor(IWeatherListInteractor weatherListInteractor) {
        mWeatherListInteractor = weatherListInteractor;
    }

    @Override
    public Observable<Boolean> addCityByPlace(Place place) {
        return mWeatherListInteractor.addCityWeatherByPlace(place);
    }
}
