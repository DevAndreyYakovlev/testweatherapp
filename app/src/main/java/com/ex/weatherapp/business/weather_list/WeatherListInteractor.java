package com.ex.weatherapp.business.weather_list;

import android.content.Context;

import com.ex.weatherapp.data.entity.weather.WeatherEntity;
import com.ex.weatherapp.data.repositories.IWeatherRepositories;
import com.ex.weatherapp.utils.MyLocation;
import com.google.android.gms.location.places.Place;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public class WeatherListInteractor implements IWeatherListInteractor {

    private final IWeatherRepositories mWeatherRepositories;


    public WeatherListInteractor(IWeatherRepositories weatherRepositories) {
        mWeatherRepositories = weatherRepositories;

    }


    @Override
    public Observable<List<WeatherEntity>> loadWeatherList() {
        return mWeatherRepositories.loadWeatherList();
    }

    @Override
    public Observable<WeatherEntity> loadWeatherForMyLocation(MyLocation myLocation) {

        if (mWeatherRepositories.dataIsEmpty()) {
            return mWeatherRepositories.loadWeatherByMyLocation(myLocation);
        }

        return null;
    }

    @Override
    public Observable<Boolean> addCityWeatherByPlace(Place place) {
        return mWeatherRepositories.addCityWeatherByPlace(place);
    }

    @Override
    public Observable<List<WeatherEntity>> updateData() {
        return mWeatherRepositories.updateData();
    }

    @Override
    public void deleteCity(WeatherEntity weatherEntity) {
        mWeatherRepositories.deleteCity(weatherEntity);
    }
}
