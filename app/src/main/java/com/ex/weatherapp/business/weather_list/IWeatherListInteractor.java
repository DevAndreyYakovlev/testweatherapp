package com.ex.weatherapp.business.weather_list;

import com.ex.weatherapp.data.entity.weather.WeatherEntity;
import com.ex.weatherapp.utils.MyLocation;
import com.google.android.gms.location.places.Place;

import java.util.List;

import io.reactivex.Observable;


/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public interface IWeatherListInteractor {

    Observable<List<WeatherEntity>> loadWeatherList();

    Observable<WeatherEntity> loadWeatherForMyLocation(MyLocation myLocation);

    Observable<Boolean> addCityWeatherByPlace(Place place);

    Observable<List<WeatherEntity>> updateData();

    void deleteCity(WeatherEntity weatherEntity);
}
