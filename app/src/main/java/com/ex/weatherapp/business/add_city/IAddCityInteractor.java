package com.ex.weatherapp.business.add_city;

import com.google.android.gms.location.places.Place;

import io.reactivex.Observable;


/**
 * Created by Yakovlev Andrey. 8/13/2017
 */

public interface IAddCityInteractor {

    Observable<Boolean> addCityByPlace(Place place);

}
