package com.ex.weatherapp.data.entity.weather;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public class Wind extends RealmObject {

    @SerializedName("speed")
    private float speed;

    @SerializedName("deg")
    private float deg;

    @SerializedName("gust")
    private float gust;

    private String description;

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getDeg() {
        return deg;
    }

    public void setDeg(float deg) {
        this.deg = deg;
    }

    public float getGust() {
        return gust;
    }

    public void setGust(float gust) {
        this.gust = gust;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
