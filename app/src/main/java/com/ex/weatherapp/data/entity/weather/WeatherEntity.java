package com.ex.weatherapp.data.entity.weather;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public class WeatherEntity extends RealmObject {

    @SerializedName("main")
    private Main main;

    @SerializedName("coord")
    private Coord coord;

    @SerializedName("visibility")
    private int visibility;
    @PrimaryKey
    @SerializedName("name")
    private String name;

    @SerializedName("wind")
    private  Wind wind;

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WeatherEntity)) return false;

        WeatherEntity that = (WeatherEntity) o;

        if (visibility != that.visibility) return false;
        return name.equals(that.name);

    }

    @Override
    public int hashCode() {
        int result = visibility;
        result = 31 * result + name.hashCode();
        return result;
    }
}
