package com.ex.weatherapp.data.repositories;

import com.ex.weatherapp.data.entity.weather.WeatherEntity;
import com.ex.weatherapp.utils.MyLocation;
import com.google.android.gms.location.places.Place;

import java.util.List;

import io.reactivex.Observable;


/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public interface IWeatherRepositories {

    boolean dataIsEmpty();

    Observable<WeatherEntity> loadWeatherByCityName(String locality);

    Observable<WeatherEntity> loadWeatherByMyLocation(MyLocation myLocation);

    Observable<List<WeatherEntity>> loadWeatherList();

    Observable<Boolean> addCityWeatherByPlace(Place place);

    Observable<List<WeatherEntity>> updateData();

    void deleteCity(WeatherEntity weatherEntity);
}
