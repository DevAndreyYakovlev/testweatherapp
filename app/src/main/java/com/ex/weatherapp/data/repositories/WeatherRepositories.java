package com.ex.weatherapp.data.repositories;

import android.support.annotation.NonNull;

import com.ex.weatherapp.data.entity.weather.WeatherEntity;
import com.ex.weatherapp.utils.MyLocation;
import com.ex.weatherapp.utils.RealmProvider;
import com.ex.weatherapp.utils.WeatherApiService;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;


/**
 * Created by Yakovlev Andrey. 8/10/2017
 */

public class WeatherRepositories implements IWeatherRepositories {


    private final RealmProvider mRealmProvider;
    private final WeatherApiService mWeatherApiService;

    public WeatherRepositories(RealmProvider realmProvider, WeatherApiService weatherApiService) {
        mRealmProvider = realmProvider;
        mWeatherApiService = weatherApiService;
    }

    @Override
    public boolean dataIsEmpty() {
        return mRealmProvider.provideRealm().where(WeatherEntity.class).count() <= 0;
    }

    @Override
    public Observable<WeatherEntity> loadWeatherByCityName(String locality) {
        System.out.println();
        return null;
    }

    @Override
    public Observable<WeatherEntity> loadWeatherByMyLocation(MyLocation myLocation) {

        Map<String, String> json = new HashMap<>();
        json.put("lat", String.valueOf(myLocation.getLat()));
        json.put("lon", String.valueOf(myLocation.getLng()));
        json.put("units", "metric");
        json.put("appid", WeatherApiService.WEATHER_API_KEY);

        return mWeatherApiService.getCityWeather(json)
                .map(v -> {
                    System.out.println();
                    return v;
                })
                .doOnNext(this::saveWeatherEntityToRealm)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<WeatherEntity>> loadWeatherList() {
        Realm realm = mRealmProvider.provideRealm();

        return Observable.just(realm.copyFromRealm(realm.where(WeatherEntity.class).findAll()));
    }

    private void saveWeatherEntityToRealm(WeatherEntity weatherEntity) {
        Realm realm = mRealmProvider.provideRealm();
        realm.beginTransaction();
        if (weatherEntity.getName().equals("Kizicheskaya")) {
            weatherEntity.setName("Kazan");
        }
        weatherEntity.getWind().setDescription(windConverter(weatherEntity.getWind().getDeg()));
        realm.insertOrUpdate(weatherEntity);
        realm.commitTransaction();
        realm.close();
    }

    @NonNull
    private String windConverter(float deg) {

        String res = "";
        if (deg == 0 || deg == 360) {
            res = "С";
        } else if (deg > 0 && deg <= 45) {
            res = "СВ";
        } else if (deg > 45 && deg <= 90) {
            res = "В";
        } else if (deg > 90 && deg <= 135) {
            res = "ЮВ";
        }else if (deg > 135 && deg <= 180){
            res = "Ю";
        }else if (deg > 180 && deg <= 225){
            res = "ЮЗ";
        }else if (deg > 225 && deg <= 270){
            res = "З";
        }else if (deg > 270 && deg < 360){
            res = "СЗ";
        }
        return res;
    }

    @Override
    public Observable<Boolean> addCityWeatherByPlace(Place place) {
        Map<String, String> json = new HashMap<>();
        LatLng latLng = place.getLatLng();
        json.put("lat", String.valueOf(latLng.latitude));
        json.put("lon", String.valueOf(latLng.longitude));
        json.put("units", "metric");
        json.put("appid", WeatherApiService.WEATHER_API_KEY);

        return mWeatherApiService.getCityWeather(json)
                .map(v -> {
                    System.out.println();
                    return v;
                })
                .doOnNext(this::saveWeatherEntityToRealm)
                .map(v -> true)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<WeatherEntity>> updateData() {
        Realm realm = mRealmProvider.provideRealm();
        return Observable.just(realm.copyFromRealm(realm.where(WeatherEntity.class).findAll()))
                .flatMap(Observable::fromIterable)
                .flatMap(v -> {
                    Map<String, String> json = new HashMap<>();
                    json.put("lat", String.valueOf(v.getCoord().getLat()));
                    json.put("lon", String.valueOf(v.getCoord().getLon()));
                    json.put("units", "metric");
                    json.put("appid", WeatherApiService.WEATHER_API_KEY);

                    return mWeatherApiService.getCityWeather(json);
                }, (weatherEntity, weatherEntity2) -> {
                    saveWeatherEntityToRealm(weatherEntity2);
                    return weatherEntity2;
                })
                .toList()
                .toObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void deleteCity(WeatherEntity weatherEntity) {
        Realm realm = mRealmProvider.provideRealm();
        realm.beginTransaction();
        realm.where(WeatherEntity.class).equalTo("name", weatherEntity.getName()).findFirst().deleteFromRealm();
        realm.commitTransaction();
        realm.close();
    }
}
