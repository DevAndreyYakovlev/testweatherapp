package com.ex.weatherapp.di.add_city;

import com.ex.weatherapp.ui.add_city.view.AddCityActivity;

import dagger.Subcomponent;

/**
 * Created by Yakovlev Andrey. 8/13/2017
 */
@Subcomponent(modules = AddCityModule.class)
public interface AddCityComponent {
    void inject(AddCityActivity addCityActivity);
}
