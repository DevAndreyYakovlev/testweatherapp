package com.ex.weatherapp.di.application;

import com.ex.weatherapp.di.add_city.AddCityComponent;
import com.ex.weatherapp.di.add_city.AddCityModule;
import com.ex.weatherapp.di.weater_list.WeatherInteractorModule;
import com.ex.weatherapp.di.weater_list.WeatherListComponent;
import com.ex.weatherapp.di.weater_list.WeatherListModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */
@Component(modules = {AppModule.class, WeatherApiModule.class, WeatherInteractorModule.class})
@Singleton
public interface AppComponent {
    WeatherListComponent plus(WeatherListModule weatherListModule);
    AddCityComponent plus(AddCityModule addCityModule);
}
