package com.ex.weatherapp.di.weater_list;

import com.ex.weatherapp.business.weather_list.IWeatherListInteractor;
import com.ex.weatherapp.business.weather_list.WeatherListInteractor;
import com.ex.weatherapp.data.repositories.IWeatherRepositories;
import com.ex.weatherapp.data.repositories.WeatherRepositories;
import com.ex.weatherapp.utils.RealmProvider;
import com.ex.weatherapp.utils.WeatherApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yakovlev Andrey. 8/13/2017
 */
@Module
public class WeatherInteractorModule {

    @Provides
    @Singleton
    IWeatherListInteractor provideWeatherListInteractor(IWeatherRepositories weatherRepositories){
        return new WeatherListInteractor(weatherRepositories);
    }

    @Provides
    @Singleton
    IWeatherRepositories provideWeatherRepositories(RealmProvider realmProvider, WeatherApiService weatherApiService){
        return new WeatherRepositories(realmProvider, weatherApiService);
    }

}
