package com.ex.weatherapp.di.weater_list;

import android.content.Context;

import com.ex.weatherapp.business.weather_list.IWeatherListInteractor;
import com.ex.weatherapp.business.weather_list.WeatherListInteractor;
import com.ex.weatherapp.data.repositories.IWeatherRepositories;
import com.ex.weatherapp.data.repositories.WeatherRepositories;
import com.ex.weatherapp.ui.weather_list.presenter.IWeatherListPresenter;
import com.ex.weatherapp.ui.weather_list.presenter.WeatherListPresenter;
import com.ex.weatherapp.utils.RealmProvider;
import com.ex.weatherapp.utils.WeatherApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */
@Module
public class WeatherListModule {

    @Provides
    @WeatherScope
    IWeatherListPresenter provideWeatherListPresenter(IWeatherListInteractor weatherListInteractor){
        return new WeatherListPresenter(weatherListInteractor);
    }


}
