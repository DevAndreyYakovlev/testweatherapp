package com.ex.weatherapp.di.weater_list;

import com.ex.weatherapp.ui.weather_list.view.WeatherActivity;


import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */
@Subcomponent(modules = {WeatherListModule.class})
public interface WeatherListComponent {
    void inject(WeatherActivity weatherActivity);
}
