package com.ex.weatherapp.di.application;

import android.content.Context;
import android.support.annotation.NonNull;

import com.ex.weatherapp.business.weather_list.IWeatherListInteractor;
import com.ex.weatherapp.business.weather_list.WeatherListInteractor;
import com.ex.weatherapp.data.repositories.IWeatherRepositories;
import com.ex.weatherapp.di.weater_list.WeatherScope;
import com.ex.weatherapp.utils.RealmProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yakovlev Andrey. 8/10/2017
 */
@Module
public class AppModule {

    private final Context appContext;

    public AppModule(@NonNull Context context) {
        appContext = context;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return appContext;
    }



    @Provides
    @Singleton
    RealmProvider provideRealmProvider(){
        return new RealmProvider();
    }



}
