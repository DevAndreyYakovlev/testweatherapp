package com.ex.weatherapp.di.add_city;

import com.ex.weatherapp.business.add_city.AddCityInteractor;
import com.ex.weatherapp.business.add_city.IAddCityInteractor;
import com.ex.weatherapp.business.weather_list.IWeatherListInteractor;
import com.ex.weatherapp.ui.add_city.presenter.AddCityPresenter;
import com.ex.weatherapp.ui.add_city.presenter.IAddCityPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yakovlev Andrey. 8/13/2017
 */
@Module
public class AddCityModule {

    @Provides
    IAddCityPresenter provideAddCityPresenter(IAddCityInteractor addCityInteractor){
        return new AddCityPresenter(addCityInteractor);
    }
    @Provides
    IAddCityInteractor provideAddCityInteractor(IWeatherListInteractor weatherListInteractor){
        return new AddCityInteractor(weatherListInteractor);
    }

}
